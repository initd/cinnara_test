#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Rodion Promyshlennikov <rodion.prm@gmail.com> {matrix or initd}'

import sys
sys.path.append('/opt/cinnara/tcp_json_rpc_common/')

import socket
import argparse
from framing_protocol import DynLengthFramingProtocolMixIn
from json_rpc import JsonRpcConstructor


class Client(DynLengthFramingProtocolMixIn):
    """
    An tcp-socket client that allows to send multiple messages to the server.
    """
    def __init__(self, host, port):
        DynLengthFramingProtocolMixIn.__init__(self)
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((self.host, self.port))
        self.json_rpc_constructor = JsonRpcConstructor()

    def help(self):
        """
        Prints help
        """
        print 'To quit enter blank line or "quit" or "exit"'
        print 'Help result presents REMOTE methods list with parameters.'
        print '''Basic formats for call methods are:
                "method" - without params
                "method param1 param2" - with 2 params
                "method param1 param2 ... paramN"  - with many params.\n'''

    def run(self):
        """
        Basic lifecycle method of client
        Running while user not entering exit(quit) or an empty string.
        """
        print 'First try to send "help" to view available remote methods.'
        self.help()
        while True:
            # read command from keyboard
            line = raw_input("-->")
            if not line or line.upper() == 'QUIT' or line.upper() == 'EXIT':
                print 'Exiting...'
                break
            if line.upper() == 'HELP':
                self.help()

            request = self.json_rpc_constructor.construct(line)
            self.send_msg(request.serialize())
            data = self.recv_msg()
            print data
        self.sock.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-host", help="Host address or domain to connect",
                        default='localhost')
    parser.add_argument("-p", "--port", help="Port to connect", type=int,
                        default=51234)
    args = parser.parse_args()
    client = Client(args.host, args.port)
    client.run()