#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Rodion Promyshlennikov <rodion.prm@gmail.com> {matrix or initd}'

import sys
sys.path.append('/opt/cinnara/tcp_json_rpc_common/')

import select
import socket
import argparse
import threading
from framing_protocol import DynLengthFramingProtocolMixIn
from json_rpc import FixedRpcDispatcher


class ThreadingTcpServer:
    """
    An server that uses threads to handle multiple clients at a time.
    """
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.backlog = 5
        self.server = None
        self.clients = []

    def open_socket(self):
        """
        Connecting and listening on host:port
        """
        try:
            self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.server.bind((self.host, self.port))
            self.server.listen(5)
        except socket.error, (value, message):
            if self.server:
                self.server.close()
            print "Could not open socket: " + message
            sys.exit(1)

    def run(self):
        """
        Main process, based non-blocking lib select
        Spawning threads for new clients
        """
        self.open_socket()
        input_src = [self.server, sys.stdin]
        running = True
        while running:
            inpready, outpready, exceptready = select.select(input_src, [], [])

            for sock in inpready:

                if sock == self.server:
                    # handle client
                    client = ClientHandler(self.server.accept())
                    client.start()
                    self.clients.append(client)

                elif sock == sys.stdin:
                    # handle standard input
                    sys.stdin.readline()
                    running = False

        self.shutdown()

    def shutdown(self):
        """
        Closing server socket, closing all client threads
        """
        self.server.close()
        for client in self.clients:
            client.join()


class ClientHandler(DynLengthFramingProtocolMixIn, threading.Thread):
    """
    Threading model based client handler class
    """
    def __init__(self, (sock, address)):
        threading.Thread.__init__(self)
        self.sock = sock
        self.address = address
        self.task_dispatcher = TaskDispatcher()

    def run(self):
        running = True
        while running:
            data = self.recv_msg()
            if data:
                data = self.task_dispatcher.parse_request(data)
                result = self.task_dispatcher.dispatch(data)
                self.send_msg(result.serialize())
            else:
                self.sock.close()
                running = False


class TaskDispatcher(FixedRpcDispatcher):
    """
    Main task dispatching class:
    parse rpc-json, call needed method, error handling.
    """
    @staticmethod
    def task_add(x, y):
        return [float(x) + float(y)]

    @staticmethod
    def task_add_multi(*args):
        result = 0
        for number in args:
            result += float(number)
        return [result]

    @staticmethod
    def task_substract(x, y):
        return [float(x) - float(y)]

    @staticmethod
    def task_multiply(x, y):
        return [float(x) * float(y)]

    @staticmethod
    def task_multiply_multi(*args):
        result = 1
        for number in args:
            result *= float(number)
        return [result]

    @staticmethod
    def task_division(x, y):
        return [float(x) / float(y)]

    @staticmethod
    def task_echo(*args):
        return [str(element) for element in args]

    @staticmethod
    def task_void():
        return

    def __init__(self):
        FixedRpcDispatcher.__init__(self)
        self.tasks = []
        self.bind_tasks()

    def bind_tasks(self):
        """
        Dynamically add available
        task(static with prefix task_) methods to dispatcher
        :return: None
        """
        tasks = [getattr(self, method) for method in dir(self)
                 if callable(getattr(self, method))]
        self.tasks = [task for task in tasks
                      if task.__name__.startswith('task_')]
        for task in self.tasks:
            self.add_method(task, task.__name__[5:])
        self.add_method(self.help)

    def help(self):
        """
        Making list of available tasks for client
        :return: list with available tasks and params
        """
        task_list = [(task.__name__[5:], task.func_code.co_varnames)
                     for task in self.tasks]
        result = []
        for task, params in task_list:
            if not params:
                result.append(task)
            elif params[0] == 'args':
                result.append('%s param1 param2 ... paramN' % task)
            else:
                result.append(task + ' ' + ' '.join(params))

        return result


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-host", help="Host address or domain to connect",
                        default='')
    parser.add_argument("-p", "--port", help="Port to connect", type=int,
                        default=51234)
    args = parser.parse_args()
    server = ThreadingTcpServer(args.host, args.port)
    server.run()