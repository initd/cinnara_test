#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Rodion Promyshlennikov <rodion.prm@gmail.com> {matrix or initd}'

from tinyrpc.protocols.jsonrpc import JSONRPCProtocol
from tinyrpc import MethodNotFoundError, ServerError
from tinyrpc.dispatch import RPCDispatcher


class FixedRpcDispatcher(RPCDispatcher):
    def __init__(self):
        RPCDispatcher.__init__(self)
        self.rpc_proto_handler = JSONRPCProtocol()

    def _dispatch(self, request):
        try:
            try:
                method = self.get_method(request.method)
            except KeyError as e:
                return request.error_respond(MethodNotFoundError(e))

            # we found the method
            try:
                if request.args and request.kwargs:
                    result = method(*request.args, **request.kwargs)
                elif request.args:
                    result = method(*request.args)
                elif request.kwargs:
                    result = method(**request.kwargs)
                else:
                    result = method()
            except Exception as e:
                # an error occured within the method, return it
                return request.error_respond(e)

            # respond with result
            return request.respond(result)
        except Exception as e:
            # unexpected error, do not let client know what happened
            return request.error_respond(ServerError())

    def parse_request(self, raw_request):
        return self.rpc_proto_handler.parse_request(raw_request)


class JsonRpcConstructor(JSONRPCProtocol):
    def __init__(self):
        JSONRPCProtocol.__init__(self)

    def construct(self, raw_string):
        splitted_input = raw_string.split()
        method = splitted_input[0]
        args = splitted_input[1:]
        return self.create_request(method, args)


def test():

    def add(x, y):
        return [float(x) + float(y)]

    def echo(*args):
        return [str(element) for element in args]

    def void():
        return

    dispatcher = dict()
    rpc_proto = JSONRPCProtocol()
    dispatcher_tiny = FixedRpcDispatcher()
    dispatcher_tiny.add_method(add)
    dispatcher_tiny.add_method(echo)
    dispatcher_tiny.add_method(void)
    requests = []
    constructor = JsonRpcConstructor()
    requests.append(constructor.construct('echo 5 3 "3" "'"5"'" dfsdfdsf "dfdsfsf"'))
    requests.append(constructor.construct('add 5 3 "3" "'"5"'" dfsdfdsf "dfdsfsf"'))
    requests.append(constructor.construct('add 5 3 '))

    dispatcher['add'] = add
    request = rpc_proto.create_request('add', ("3", 4))
    requests.append(request)
    print request.serialize()
    result = dispatcher[request.method](*request.args)
    response = request.respond(result)
    print response.serialize()

    dispatcher['echo'] = echo
    request = rpc_proto.create_request('echo', (3, 4, 'dsfdf,', 454))
    requests.append(request)
    print request.serialize()
    result = dispatcher[request.method](*request.args)
    response = request.respond(result)
    print response.serialize()

    dispatcher['void'] = void
    request = rpc_proto.create_request('void')
    requests.append(request)
    print request.serialize()
    result = dispatcher[request.method]()
    response = request.respond(result)
    print response.serialize()

    print "___________________________________________"

    for request in requests:
        print request.serialize()
        response = dispatcher_tiny.dispatch(request)
        print response.serialize()




if __name__ == '__main__':
    test()