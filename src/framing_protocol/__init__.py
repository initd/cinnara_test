#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Rodion Promyshlennikov <rodion.prm@gmail.com> {matrix or initd}'

import struct
import socket


class DynLengthFramingProtocolMixIn:
    """
    MixIn implements dynamic length message protocol
    """
    def __init__(self):
        self.sock = None

    def _recvall(self, count):
        buf = ''
        while count:
            try:
                newbuf = self.sock.recv(count)
            except MemoryError, socket.error:
                self.sock.close()
                return
            if not newbuf:
                return None
            buf += newbuf
            count -= len(newbuf)
        return buf

    def send_msg(self, data):
        length = len(data)
        self.sock.sendall(struct.pack('!I', length))
        self.sock.sendall(data)

    def recv_msg(self):
        lengthbuf = self._recvall(4)
        if not lengthbuf:
            return
        length, = struct.unpack('!I', lengthbuf)
        return self._recvall(length)


def main():
    pass


if __name__ == '__main__':
    main()