#!/bin/bash
mkdir -p tcp_json_rpc_server/opt/cinnara/tcp_json_rpc_server
mkdir -p tcp_json_rpc_client/opt/cinnara/tcp_json_rpc_client
mkdir -p tcp_json_rpc_common/opt/cinnara/tcp_json_rpc_common/json_rpc
mkdir -p tcp_json_rpc_common/opt/cinnara/tcp_json_rpc_common/framing_protocol
cp -R ../src/py_json_server/* tcp_json_rpc_server/opt/cinnara/tcp_json_rpc_server/
chmod +x tcp_json_rpc_server/opt/cinnara/tcp_json_rpc_server/*.py
cp -R ../src/py_json_client/* tcp_json_rpc_client/opt/cinnara/tcp_json_rpc_client/
chmod +x tcp_json_rpc_client/opt/cinnara/tcp_json_rpc_client/*.py
cp -R ../src/json_rpc/* tcp_json_rpc_common/opt/cinnara/tcp_json_rpc_common/json_rpc/
cp -R ../src/framing_protocol/* tcp_json_rpc_common/opt/cinnara/tcp_json_rpc_common/framing_protocol/
./md5sums.sh
./make_debs.sh
mkdir -p ../bin
mv *.deb ../bin