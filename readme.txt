# ﻿1. Description: #
	py-tcp-json-rpc-client and py-tcp-json-rpc-server
	(compatible to python v.2.7)

	## 1.1. Server application (py-tcp-json-rpc-server package(tcp_json_rpc_server.deb)) ## 
	Implements server application supporting JSON based protocol over TCP/IP.  JSON schema contain following attributes (JSON-RPC 2.0 standard)):
	ex.: {"params": ["2", "3", "4", "A", "B", "C"], "jsonrpc": "2.0", "method": "echo", "id": 3}
		 {"jsonrpc": "2.0", "id": 3, "result": ["2", "3", "4", "A", "B", "C"]}
	"standard ident" - "jsonrpc": "2.0"
	"request id" - "id"
	"command name" - "method",
	"array of input parameters" - "params",
	"array of output parameters" - "result"


	Some commands with specific input and output parameters were be implemented (see next in client CLI syntax, enter without quotes)):
		"help"
		"add x y", 
		"add_multi param1 param2 ... paramN", 
		"division x y", 
		"echo param1 param2 ... paramN", 
		"multiply x y", 
		"multiply_multi param1 param2 ... paramN", 
		"substract x y", 
		"void"
		
	Server responding on commands and send the client the predefined( or calculated)) array of output parameters. All errors are handling and properly processed in accordance with the standard JSON-RPC 2.0.

	Server supporting multi-thread(Threading.Threads for ClientHandlers)) not blocking architecture(based on select.select()))) to process any number of connections. 
	The connection initiated by client are keep alive until a client closes it.

	## 1.2. Client application py-tcp-json-rpc-client package(tcp_json_rpc_client.deb) ##
	Client application supporting protocol described in Part 1. 
	Client controlled by command line interface(CLI)). 
	The commands that are going to be sent to server entered as simple string with command name and input parameters.
	ex.: echo 2 3 4 A B C
	Output received from server showing as JSON formatted strings.
	ex.: {"jsonrpc": "2.0", "id": 3, "result": ["2", "3", "4", "A", "B", "C"]}


	In py-tcp-json-rpc-common package(tcp_json_rpc_common.deb) are placed two common modules with helping classes for client\server apps. In first module implemented framing_protocol over tcp/ip for client<->server messaging.
	In second module are helping classes based on external lib tinyrpc. Tinyrpc need to be installed before running this apps.

	Applications implemented with classes, exceptions and contexts.
	Server and Clients shipped as debian packages. There are building system for all parts of apps.



# 2. Instructions: # 
	
	## 2.1. Folder structure: ##
		### 2.1.1. Sources structure: ###
	cinnara_test/
			├── bin
			│   ├── tcp_json_rpc_client.deb
			│   ├── tcp_json_rpc_common.deb
			│   └── tcp_json_rpc_server.deb
			├── build
			│   ├── build.sh
			│   ├── make_debs.sh
			│   ├── md5sums.sh
			│   ├── tcp_json_rpc_client
			│   │   ├── DEBIAN
			│   │   │   ├── control
			│   │   │   ├── dirs
			│   │   │   └── md5sums
			│   │   └── opt
			│   │       └── cinnara
			│   │           └── tcp_json_rpc_client
			│   │               └── py_tcp_json_client.py
			│   ├── tcp_json_rpc_common
			│   │   ├── DEBIAN
			│   │   │   ├── control
			│   │   │   ├── dirs
			│   │   │   └── md5sums
			│   │   └── opt
			│   │       └── cinnara
			│   │           ├── tcp_json_rpc_client
			│   │           ├── tcp_json_rpc_common
			│   │           │   ├── framing_protocol
			│   │           │   │   └── __init__.py
			│   │           │   └── json_rpc
			│   │           │       └── __init__.py
			│   │           └── tcp_json_rpc_server
			│   └── tcp_json_rpc_server
			│       ├── DEBIAN
			│       │   ├── control
			│       │   ├── dirs
			│       │   └── md5sums
			│       └── opt
			│           └── cinnara
			│               └── tcp_json_rpc_server
			│                   └── py_tcp_threading_server.py
			├── readme.txt
			└── src
				├── framing_protocol
				│   └── __init__.py
				├── json_rpc
				│   └── __init__.py
				├── py_json_client
				│   └── py_tcp_json_client.py
				└── py_json_server
					├── py_tcp_threading_server.py
					└── stop.sh 
		### 2.1.2. File system structure after installing: ###
		opt
		└── cinnara
			├── tcp_json_rpc_client
			│   └── py_tcp_json_client.py
			├── tcp_json_rpc_common
			│   ├── framing_protocol
			│   │   └── __init__.py
			│   └── json_rpc
			│       └── __init__.py
			└── tcp_json_rpc_server
				├── py_tcp_threading_server.py
				└── stop.sh

	

	## 2.2. Testing environment: ##
		Applications tested in next environment:
			Debian 7.8 (Linux 3.2.0-1-686-pae #1 SMP Sun Feb 5 23:52:49 UTC 2012 i686 GNU/Linux)
			Python 2.7.3
			tinyrpc==0.5 - exactly one external dependency
	
	## 2.3. To deploy: ##
		1. sudo dpkg -i tcp_json_rpc_common.deb
		2. sudo dpkg -i tcp_json_rpc_server.deb
		3. sudo dpkg -i tcp_json_rpc_client.deb
		4. apt-get install -f if .deb were be installed in invalid order
		5. apt-get install python-pip
		6. pip install tinyrpc
	
	## 2.4. To use: ##
		1. Start server: 
			/opt/cinnara/tcp_json_rpc_server/py_tcp_threading_server.py
		2. Start some clients: 
			/opt/cinnara/tcp_json_rpc_client/py_tcp_json_client.py
			
		You can try also cd into /opt/cinnara/tcp_json_rpc_(client or server)/ folder and run ./py_tcp_threading_(client or server).py
		If python not in global env you can try also run "python py_tcp_threading_(client or server).py"
		
		3. To stop server:
			/opt/cinnara/tcp_json_rpc_server/stop.sh
		4. To stop client:
			Type in client CLI: quit, exit or enter a empty line.
			
	## 2.5. To build after changing sources (now has not support for automatic version control and changelog)): ##
		1. cd into main folder described in 2.1.1., ex. cd /home/user/development/python/cinnara_test
		2. cd into build folder
		3. make sure that build.sh has x bit (chmod +x build.sh))
		4. run ./build.sh
		5. now all .deb packages are in ../bin folder
		
	## 2.6. To uninstall: ##
		1. sudo aptitude purge py-tcp-json-rpc-common py-tcp-json-rpc-client py-tcp-json-rpc-server



# 3. Test input and output data to test applications: #
	-->add 4 34
	<--{"jsonrpc": "2.0", "id": 1, "result": [38.0]}
	-->add_multi 3 34 44 15
	<--{"jsonrpc": "2.0", "id": 2, "result": [96.0]}
	-->division 5 9
	<--{"jsonrpc": "2.0", "id": 3, "result": [0.5555555555555556]}
	-->division 5 0
	<--{"jsonrpc": "2.0", "id": 6, "error": {"message": "float division by zero", "code": -32000}}
	-->echo 2 3 4 A B C
	<--{"jsonrpc": "2.0", "id": 7, "result": ["2", "3", "4", "A", "B", "C"]}
	-->multiply 5 5.5
	<--{"jsonrpc": "2.0", "id": 8, "result": [27.5]}
	-->multiply_multi 5 5 8 12
	<--{"jsonrpc": "2.0", "id": 9, "result": [2400.0]}
	-->substract 23 55
	<--{"jsonrpc": "2.0", "id": 10, "result": [-32.0]}
	-->void
	<--{"jsonrpc": "2.0", "id": 11, "result": null}